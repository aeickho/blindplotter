include <aluminiumProfiles.scad>;
include <stepper-motors.scad>;

module laser() {
  color("red") cylinder(h=3500,r=.5);
  difference() {
    cylinder(h=35,r=6);
    cylinder(h=5,r=2);
    translate([0,0,5]) cylinder(h=30,r=5);
  }
  difference() {
    color("lightgrey") {
      cylinder(h=30,r=9);
      for(a = [1,2,3,4,5,6,7,8,9,10]) {
        color("lightgrey")
        translate([0,0,7.5])
        rotate(a*18, [0,0,1])
        cube([26,2,15], center=true);
      }
    }
    cylinder(h=36,r=6.05);
  }

}

module frame(width, frame_height, work_space) {
  ptype  = BR_20x20;
  pwidth = 20;
  cdif = (width/2+pwidth/2);

  for (a = [0, 1, 2, 3]) {
    rotate(90*a, [0,0,1]) {
      // Bein
      translate([-cdif, -cdif, 0 ])
        aluProExtrusion(ptype, l=frame_height);
      // Horizontale Verbindung
      translate([-pwidth/2, 0, pwidth/2])
      translate([-width/2, -width/2, work_space])
      rotate(90, [-1,0,0])
        aluProExtrusion(ptype, l=width);
    }
  }
}

// Wellen
for (off = [-240, -200, 200, 240]) {
  rotate(90, [1,0,0])
  translate([off,220,-240])
    cylinder(h=480, r1=5, r2=5);
}

laser();
color("lightgrey") frame(500, 300, 200);

translate([250,225,175])
rotate(90,[1,0,0])
rotate(90,[0,-1,0]) {
  NEMA(NEMA17);
  translate([0,0,15]) {
    cylinder(5,20,20);
    translate([0,0,5])
    cylinder(3,5,5);
  }
  
}
  
